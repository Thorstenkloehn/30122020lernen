# Startseite


- [Meine Projekte](./MeineProjekt.md)

## Grundlagen 

- [Installieren]()
  - [Windows](./Installieren/Windows.md)
  - [Server](./Installieren/Server.md)
    - [Hadoop](./Installieren/Hadoop.md)
    - [Apache Zeppelin](./Installieren/ApacheZeppelin.md)
  - [Linux Deskop](./Installieren/LinuxDeskop.md)
  - [Frontend](./Installieren/Frontend.md)

- [Grundlagen]()
  - [Datentypen](./Grundlagen/Datentypen.md)
    - [Einfache Datentypen](./Grundlagen/EinfacheDatentypen/EinfacheDatentypen.md)
       - [Ganzzahlige Typen](./Grundlagen/EinfacheDatentypen/GanzzahligeTypen.md)
       - [Kommazahlen Typen](./Grundlagen/EinfacheDatentypen/KommazahlenType.md)
       - [Wahrheitswerte Typen](./Grundlagen/EinfacheDatentypen/wahrheitswertetypen.md)
       - [Aufzählungstypen](./Grundlagen/EinfacheDatentypen/Aufzaehlungstypen.md)
       - [Leeredatentypen](./Grundlagen/EinfacheDatentypen/Leeredatentypen.md)
       - [Zeichen](./Grundlagen/EinfacheDatentypen/zeichen.md)
      - [Zeigertypen](./Grundlagen/Zeigertypen.md)
        - [Einfache Zeiger](./Grundlagen/Zeiger/EinfacheZeiger.md)
     - [Struct](./Grundlagen/struct/struct.md)
     - [Datensatz](./Grundlagen/Datensatz.md)
    
  - [Programmfluss]()
    - [Entscheidung und Logik]()
    - [Schleifen]()
    
  - [Funktion]()   
     - [Lambda-Funktion]()
     - [Inline Funktion]()
     - [async function ]()
     - [Anonyme Funktion]()
  
- [OOP]()
   - [Klasse]()
   - [Klassenlose OOP]()
  
- [Postgres]()

## Webprogrammierung

- [Webprogrammierung mit Go](./WebprogrannierungGo/Grundlagen.md)
   - [Teil 1 Anfrage bearbeiten](./WebprogrannierungGo/Teil1Anfragenbearbeiten.md)

## Resource

- [Ressource](./Ressource.md)

## Rechtlichte Angaben
- [Rechtlichte Angaben]()
  - [Impressum](./Impressum.md)
  - [Datenschutzerklärung](./Datenschutzerklärung.md)