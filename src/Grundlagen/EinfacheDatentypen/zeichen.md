# Zeichen
## Go
```go

package main

import "fmt"




func main() {


	var text byte= 'w'


	fmt.Println("Hallo Leute",text)
}


```
## C+

```cpp
#include <iostream>

char zahl = 't';

int main() {


    std::cout << zahl << std::endl;
    return 0;
}


```
## Rust 

````rust
fn main() {
    let c:char ='t';
    println!("Zeichen {}",c);
}

````
