# Ganzzahlige Typen


## C++


| Art       | Speichergröße          | Wertebereich  |
| ------------- |:-------------:| -----:|
|char| 1 Byte|	-127 bis 127 oder 0 bis 255|
|unsigned char|1 Byte |	0 bis 255|
|signed char|1 Byte|-127 bis 127|
|int|4 Bytes|	-2147483648 bis 2147483647|
|unsigned int|	4 Bytes	| 0 bis 4294967295|
|signed int|	4 Bytes|	-2147483648 bis 2147483647|
|short int|	2 Bytes|	-32768 bis 32767|
|unsigned short int| 2 Bytes|	0 bis 65,535|
|signed short int|	2 Bytes|	-32768 bis 32767|
|long int| 8 Bytes|-2,147,483,648 bis 2,147,483,647|
|unsigned long int| 8 Bytes|	0 bis 4,294,967,295|
|long long int|8 Bytes|	-(2^63) bis (2^63)-1|
|unsigned long long int| 8 Bytes|	0 bis 18,446,744,073,709,551,615|



## Java 

| Art       | Speichergröße          | Wertebereich  |
| ------------- |:-------------:| -----:|
|byte | 1 Byte | -128 bis 127|
|short|	2 Bytes | -32,768 bis 32,767|
|int|	4 Bytes | -2,147,483,648 bis 2,147,483,647|
|long|  8 bytes | -9,223,372,036,854,775,808 bis 9,223,372,036,854,775,807|


## Rust

|Art|Wertbereich
| ------------- |:-------------:|
|i8	|-128 bis	127|
|u8 |0	bis 255|
|i16	|-32768 bis	32767|
|u16 | 	0	bis 65535 |
|i32	 |-2147483648 bis 2147483647|
|u32	| 0	bis 4294967295|
|i64	 |9223372036854775808 bis 9223372036854775807|
|u64	| 0	bis 18446744073709551615|
|i128 |-170141183460469231731687303715884105728 bis	170141183460469231731687303715884105727|
|u128 |0	bis 340282366920938463463374607431768211455|


## Go

| Art       | Speichergröße          | Wertebereich  |
| ------------- |:-------------:| -----:|
|uint8 |Bit 8 | 0 bis 255|
|uint16| 16 Bit| 0 bis 65535|
|uint32|32 Bit  |0 bis 4294967295|
|uint64|64 Bit |0 bis 18446744073709551615|
|int8|8 Bit |-128 bis 127|
|int16| 16 Bit |-32768 bis 32767|
|int32|32 Bit|-2147483648 bis 2147483647|
|int64|64 Bit|-9223372036854775808 bis 9223372036854775807|




## Python 

```python

counter = 100         

```
## Javascript

```Javascript


pubnkt = 100; 

```