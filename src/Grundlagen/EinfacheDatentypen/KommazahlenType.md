# Kommazahlen Typen


## C++ 

| Art       | Speichergröße          | 
| ------------- |:-------------:|
|float|	4 Bytes|
|double|	8 Bytes|
|long double|12 Bytes|	


## JAVA
| Art       | Speichergröße          | 
| ------------- |:-------------:|
|float|	4 Bytes|
|double|	8 Bytes|


## Rust
| Art       | Speichergröße          | 
| ------------- |:-------------:|
|f32|	4 Bytes|
|f64|	8 Bytes|

## Go
| Art       | Speichergröße          | 
| ------------- |:-------------:|
|float32|IEEE-754 32-Bit-Gleitkommazahlen|
|float64|IEEE-754 64-Bit-Gleitkommazahlen|
|complex64|Komplexe Zahlen mit float32 Real- und Imaginärteilen|
|Komplex128|Komplexe Zahlen mit float64 Real- und Imaginärteilen|

## Python 

```python

zahl = 1.989

```

## Javascript

```javascript

zahl = 1.4545

```