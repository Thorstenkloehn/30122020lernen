# Wahrheitswerte Typen


## C++

```cpp

bool wahrheitwert;
wahrheitwert = true;  //wahr
wahrheitwert = false;  //falsch

```

## Java

``` java

boolean wahreitswert ;
wahreitswert = true;
wahreitswert = false;
         
```





## Javascript

```node

wahrheit= true;
wahrheit= false;

```

## Python

```py

wahrheit= true;
wahrheit= false;

```