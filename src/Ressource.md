# Ressource
## C
* [Allgemein C](https://www.tutorialspoint.com/cprogramming)
* [C Komplettkurs: Praxisnahe Programmierung für C Einsteiger](https://www.udemy.com/course/c-programmierung-praxisnaher-komplettkurs-fur-einsteiger/)

## C++
* [Allgemein C++](https://www.tutorialspoint.com/cplusplus)
* [C++ Komplettkurs: Praxisnahe und Moderne C++ Programmierung](https://www.udemy.com/course/der-komplettkurs-zur-modernen-c-programmierung/)

## Java
* [Allgemein Java](https://www.tutorialspoint.com/java)
* [Die komplette Java 11 Masterclass -von 0 auf 100 in 6 Wochen](https://www.udemy.com/course/java-11-masterclass/)


## Rust
* [Allgemein Rust](https://www.tutorialspoint.com/rust)
* [Rustbook](https://rust-lang-de.github.io/rustbook-de/)
## Go

* [Go Allgemein](https://www.tutorialspoint.com/go)
* [Einführung in Go](https://www.udemy.com/course/einfuhrung-in-go/)

## Javascript

* [Allgemein Javascript](https://www.tutorialspoint.com/javascript)
* [Die komplette JavaScript Masterclass: Vom Anfänger zum Profi](https://www.udemy.com/course/javascript-komplett/)

## Python

* [Allgemein Python ](https://www.tutorialspoint.com/python)
* [Python Bootcamp: Vom Anfänger zum Profi, inkl. Data Science](https://www.udemy.com/course/python-bootcamp/)

## Datenbank

### PostgreSQL Datenbank

* [PostgreSQL Allgemein](https://www.tutorialspoint.com/postgresql/)
* [Die komplette SQL Masterclass: Vom Anfänger zum Profi](https://www.udemy.com/course/sql-komplett/)

### MongoDB
### Cassandra
### Neo4j
